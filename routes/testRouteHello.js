const express = require('express')
router = express.Router()
hello = require('../controllers/ControllerHello')

router.get('/', ControllerHello.hello)

module.exports = router;