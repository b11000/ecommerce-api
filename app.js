const express = require('express'),
app = express()

app.use('/', require('./routes/testRouteHello'))


app.get('/', (request, response) => {
    response.send('hello pokemon')
})

const PORT = 8000;

app.listen(PORT,()=> {
    console.log(`Connected to ${PORT}`)
})